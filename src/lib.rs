// #![allow(unused)]
use std::{
    sync::{mpsc, Arc, Mutex},
    thread,
};

/// A thread pool
///
/// The size is the number of threads in the pool
pub struct ThreadPool {
    // threads: Vec<thread::JoinHandle<()>>,
    workers: Vec<Worker>,
    // sender: mpsc::Sender<Job>,
    sender: Option<mpsc::Sender<Job>>,
}

// struct Job;
type Job = Box<dyn FnOnce() + Send + 'static>;

impl ThreadPool {
    /// Create a new ThreadPool
    ///
    /// The size is the number of threads in the pool
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));

        // let mut threads = Vec::with_capacity(size);
        let mut workers = Vec::with_capacity(size);

        // for _ in 0..size {
        for id in 0..size {
            // create some threads and store them in the vector
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        // ThreadPool { threads } // Vec's type inference
        ThreadPool {
            workers,
            // sender
            sender: Some(sender),
        }
    }

    /// Send a job to the thread pool
    ///
    /// # Panics
    ///
    /// The `execute` function will panic if the sender is None, or if the job isn't send
    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender
            .as_ref()
            .expect("The sender is None")
            .send(job)
            .expect("Failed to send job");
        // if let Some(sender) = self.sender.take() {
        //     sender.send(job).unwrap();
        // }
    }
}

// wait until all threads are done if threadpool is dropped
impl Drop for ThreadPool {
    fn drop(&mut self) {
        drop(self.sender.take());
        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);
            if let Some(thread) = worker.thread.take() {
                thread.join().expect("Failed to join thread");
            }
        }
    }
}

struct Worker {
    id: usize,
    // thread: thread::JoinHandle<()>,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Job>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            // let job = receiver.lock().unwrap().recv().unwrap();
            let message = receiver.lock().expect("Failed to lock receiver").recv();

            match message {
                Ok(job) => {
                    println!("Worker {id} got a job; executing.");
                    job();
                }
                Err(_) => {
                    println!("Worker {id} disconnected; shutting down.");
                    break;
                }
            }
        });

        Worker {
            id,
            // thread
            thread: Some(thread),
        }
    }
}
